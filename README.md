## What

[swagger2ts-gen-cli](https://gitee.com/gengkaibo/swagger2ts-gen-cli) 是一个基于 Nodejs + Rollup + TypeScript 等技术开发，可根据后端服务的 swagger-api 自动化生成客户端代码的 Cli 工具。

## Why

在[这个背景 ♨️](https://mp.weixin.qq.com/s/gEFpMGhrmFj8f3GybRBmOQ)下产生了编写自动化生成工具的想法

## Requirements

- Node version (v16.0.0+)
- 外网环境支持

## How

1. 使用 `npm` 或 `pnpm` 安装
   `npm install swagger2ts-gen-cli -D`
   `pnpm add swagger2ts-gen-cli -D`

2. 安装完毕后直接在命令行输入 `swagger2ts-gen-cli` 或 `sg-cli` 可查看命令，如果执行失败可添加 `npx` 前缀试试

```cmd
  Options:
    -V, --version          output the version number
    -h, --help             display help for command
  Commands:
    gen [options]          根据后端服务的 swagger-api 生成客户端代码
    help [command]         display help for command
```

3. `npx swagger2ts-gen-cli gen -h` 查看 `gen` 命令需要的参数

```cmd
跟据后端服务的 swagger-api 生成客户端代码
  Options:
    -url, --url <url>                 swagger-config 或 swagger-resources 或 api-docs 的路径, 例如: http://xxx.xxx.xxx.xxx:xxxx/v3/api-docs/swagger-config 或 http://xxx.xxx.xxx.xxx:xxxx/swagger-resources 或 http://xxx.xxx.xxx.xxx:xxxx/api/auth/v3/api-docs'
    -c, --convert                     是否需要转换为 openApi3.0 标准 (default: false)
    -ct, --client_type [client_type]  要生成的客户端代码文件类型 (default: "typescript-axios")
    -o, --output_path [output_path]  生成客户端代码文件后存放的位置 (default: "./swagger2ts-generated")
    -clean, --clean  是否清理解压后的无效文件 (default: false)
    -h, --help                        display help for command
```

其中 `--client_type` 支持的参数有如下

- Swagger 2

![openapi-v2-client](https://gengkaibo.github.io/swagger-editor-md/images/openapi-v2-client.png)

- OpenAPI 3

![openapi-v3-client](https://gengkaibo.github.io/swagger-editor-md/images/openapi-v3-client.png)

### 示例

1. 💡 直接将 Swagger 2 标准的 API 生成相应客户端类型的文件

```cmd
sg-cli gen -url http://172.18.10.58:30042/swagger-resources -ct typescript-fetch -o ./swagger2ts-generated/v2 -clean
```

2. 💡 直接将 OpenAPI 3 标准的 API 生成相应客户端类型的文件

```cmd
sg-cli gen -url http://172.18.10.28:30060/v3/api-docs/swagger-config -ct typescript-axios -o ./swagger2ts-generated/v3 -clean
```

3. 💡 将 Swagger 2 先转换成 OpenAPI 3 标准 API，再生成相应客户端类型的文件

```cmd
sg-cli gen -url http://172.18.10.58:30042/swagger-resources -c -ct typescript-axios -o ./swagger2ts-generated/v2to3 -clean
```

## 问题交流

⚡️ 可以扫描下方微信公众号私信我

![微信公众号](https://gengkaibo.github.io/chat-public.jpg)

🌟 如果你也喜欢这个项目，请给我一个 star。你的鼓励就是我最大的动力！

## 依赖库介绍

### commander

> commander 是一个用于解析命令行参数和生成命令行界面的 Node.js 库。它提供了一组强大的 API，可以轻松创建 CLI 工具、自动化脚本、服务器端应用程序等各种类型的 Node.js 程序。该库支持定义参数、选项和子命令，并在程序运行时轻松解析这些内容。它还提供了参数验证、帮助信息生成、命令行提示符等功能，是 Node.js 社区中最流行的命令行开发库之一。

### ora

> ora 是一个用于 Node.js 的命令行加载指示器库。在执行长时间的任务或等待 IO 操作完成时，这个库可以在终端上显示一个动态的加载指示器，让用户知道程序正在运行，并且不会误以为程序已经挂起了。ora 库支持自定义加载指示器的样式和颜色，可以根据需要创建旋转、箭头、进度条和等待图标等多种样式。此外，该库还支持 promise 和 async/await 的方式调用，使得编写异步代码更加方便。通过简单易用的 API，ora 库已经成为 Node.js 社区中最受欢迎的命令行加载指示器库之一，被广泛应用于各种类型的 CLI 工具、自动化脚本和其他 Node.js 应用程序中。

### fs-extra

> fs-extra 是一个对 Node.js 原生文件系统模块(fs)进行扩展和增强的 Node.js 库。它提供了更多的功能和操作，如删除目录、拷贝文件、读取 json 文件、文件重命名等等。相对于原生的 fs 模块，fs-extra 库对使用者的友好度更高，封装了常用的操作，易于理解和使用。

### rimraf

> rimraf 是一个用于 Node.js 的文件删除工具。它提供了一个强大的 API，可以递归地删除整个目录树和其中的所有文件。它也支持 Windows 平台上的路径，可以在不同的操作系统环境下工作。相对于原生的 fs 模块，它提供了更高层次的封装，使用起来更加方便，并且在处理较大的目录时效率更高。

### js-yaml

> js-yaml 是一个 JavaScript 库，用于解析和序列化 YAML（"YAML Ain't Markup Language"）格式的数据。它支持 YAML 1.2 规范，并且可以在浏览器端和服务器端运行。通过使用 js-yaml，开发者可以方便地将 YAML 格式的文本转换为 JavaScript 对象，以便于在程序中进行操作。它还可以将 JavaScript 对象转换为 YAML 格式的文本，以便于将数据存储到文件或网络中。js-yaml 的安装非常简单，只需要在命令行中运行 npm i js-yaml 命令即可安装。在安装完成后，我们可以通过 require("js-yaml") 的方式引入该库，然后使用其中提供的方法来进行解析和序列化操作。据 npmjs.com 和 GitHub 上的信息显示，js-yaml 是一个非常流行的 JavaScript 库，在 npm 包管理器中有超过 16,900 个项目正在使用它，并且它也得到了众多开发者的关注和支持。

### axios

> axios 是一个基于 Promise 的 HTTP 客户端，可以用在浏览器和 node.js 中，能够支持请求的拦截、响应的拦截、请求取消、自动转换 JSON 数据等功能。它具有极为简单的 API 接口且易于上手，并且支持 TypeScript。axios 最初是由一个名为 Matt Zabriskie 的开发者创建的，后来被合并到 JavaScript 库之中。它已经成为了越来越多的开发者们使用的首选框架之一，广泛应用于各种 Web 开发场景。使用 axios，我们可以很方便地发起 GET、POST、PUT、DELETE 等各种类型的 HTTP 请求，并且能够自定义 headers, timeout 以及其他一些配置选项。另外，在与 Promise 结合使用时，我们可以通过 Promise 的异常处理机制来处理请求过程中出现的错误，使代码结构更加清晰简洁。

### chalk

> chalk 是一个在终端输出时可以对输出样式进行设置的 Node.js 第三方库。其可以用来为终端输出添加颜色和样式，如加粗、下划线等，从而使终端输出更具可读性。chalk 库提供了丰富的 API，提供了多种样式和颜色选项，并且是基于 ANSI 转义码实现的。

### adm-zip

> adm-zip 是一个纯 JavaScript 实现的 Zip 数据压缩库，用于在 Node.js 应用中对 Zip 文件进行创建、解压缩操作。它支持将压缩文件直接写入到磁盘或内存缓冲区。adm-zip 还提供了一组 API 方法和属性，用于读取、修改以及添加条目和文件。adm-zip 提供了一个简单易用的 API，使用 npm 包管理器可以很方便地安装和发布。同时，它也比其他基于 C 的 Zip 库更快，因为它是一个纯 JavaScript 实现的库。该库还具有优秀的跨平台兼容性，可以在多个操作系统平台上运行。
