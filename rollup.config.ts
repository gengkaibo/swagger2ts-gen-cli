import typescript from 'rollup-plugin-typescript2'
import { terser } from 'rollup-plugin-terser'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import json from '@rollup/plugin-json'
import { defineConfig } from 'rollup'
const isProd = process.env.NODE_ENV == 'production'
export default defineConfig({
  input: [
    'src/index.ts',
    'src/main.ts',
  ],
  plugins: [
    nodeResolve(),
    json(),
    typescript(),
    isProd && terser()
  ],
  external: ['commander', 'ora', 'chalk', 'axios', 'fs-extra', 'js-yaml', 'rimraf', 'adm-zip'],
  output: {
    dir: 'dist'
  },
})
