/**
 * @description 处理某些字段的导致的codegen接口服务报错
 */
export function codeGenErrorHandler(data: any){
  if(data?.definitions){
    const keys = Reflect.ownKeys(data?.definitions)
    keys.forEach(key=>{
      if(key.toString().startsWith('Map')){
        Reflect.deleteProperty(data.definitions, key)
      }
      if(key.toString().startsWith('JSONObject')){
        Reflect.deleteProperty(data.definitions, key)
      }
    })
  }
  return data
}
