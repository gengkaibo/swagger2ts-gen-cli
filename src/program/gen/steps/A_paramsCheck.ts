import ora from 'ora'
import chalk from 'chalk'
import { GenOptions } from '@/tsHelpers'
export const A_paramsCheck = async (options: GenOptions) => {
  const startMsg = '读取参数中... \n'
  const errMsg = '读取参数失败 \n'
  const successMsg = '读取参数成功 \n'

  const spinner = ora(startMsg).start()
  console.log(`
    ${chalk.blue('--url:')} ${chalk.green(options.url)} ${chalk.red(typeof (options.url))} \n
    ${chalk.blue('--convert:')} ${chalk.green(options.convert)} ${chalk.red(typeof (options.convert))} \n
    ${chalk.blue('--client_type:')} ${chalk.green(options.client_type)} ${chalk.red(typeof (options.client_type))} \n
    ${chalk.blue('--output_path:')} ${chalk.green(options.output_dir)} ${chalk.red(typeof (options.output_dir))} \n
    ${chalk.blue('--clean:')} ${chalk.green(options.clean)} ${chalk.red(typeof (options.clean))} \n
  `)
  if (!options.url && !options.url.includes('swagger-config')&& !options.url.includes('swagger-resources')&& !options.url.includes('api-docs')) {
    spinner.fail(errMsg)
    throw new Error('请添加 "--url" 参数，并检查是否为后端的 swagger-config 或 swagger-resources 或 api-docs 的路径')
  }
  spinner.succeed(successMsg)
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
  return options
}
