import { Command } from 'commander'
import chalk from 'chalk'
import { pipe } from '@/utils/pipe'
import { A_paramsCheck } from './steps/A_paramsCheck'
import { B_readApiDocs } from './steps/B_readApiDocs'
import { C_convert } from './steps/C_convert'
import { D_downloadFile } from './steps/D_downloadFile'
import { GenOptions } from '@/tsHelpers'

export const gen = async (options: GenOptions) => {
  const pipeline = pipe(
    A_paramsCheck, // 第1步 读取参数并验证
    B_readApiDocs, // 第2步 读取api-docs数据
    C_convert, // 第3步 数据处理
    D_downloadFile // 第4步 生成文件
  )
  pipeline(options)
    .then(() => {
      console.log(chalk.green('Success!'))
    })
    .catch((error) => {
      console.log(chalk.red('Error!'))
      console.error(chalk.red(error))
    })
}

export const genProgram = (program: Command) => {
  program
    .command('gen')
    .description('根据后端服务的 swagger-api 生成客户端代码')
    .option(
      '-url, --url <url>',
      'swagger-config 或 swagger-resources 或 api-docs 的路径, 例如: http://xxx.xxx.xxx.xxx:xxxx/v3/api-docs/swagger-config 或 http://xxx.xxx.xxx.xxx:xxxx/swagger-resources 或 http://xxx.xxx.xxx.xxx:xxxx/api/auth/v3/api-docs'
    )
    .option(
      '-c, --convert',
      '是否需要转换为 openApi3.0 标准',
      false
    )
    .option(
      '-ct, --client_type [client_type]',
      '要生成的客户端代码文件类型',
      'typescript-axios'
    )
    .option(
      '-o, --output_dir [output_dir]',
      '生成客户端代码文件后存放的位置',
      './swagger2ts-gen'
    )
    .option(
      '-clean, --clean',
      '是否清理解压后的无效文件',
      false
    )
    .action(async (options: GenOptions) => {
      gen(options)
    })
}
