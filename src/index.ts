import { version } from '../package.json'
import { Command } from 'commander'
import chalk from 'chalk'
import { genProgram } from './program/gen'
const program = new Command()
program.version(chalk.green.bold(`swagger2ts-gen-cli@${version}`))

genProgram(program)

program.parse(process.argv)
