/**
 * 获取swagger 2.0 对应可生成的 client 类型
 */
export const swagger2ClientsUrl = 'https://generator.swagger.io/api/gen/clients' // get

/**
 * swagger 2.0 生成 client
*/
export const swagger2GenUrl = 'https://generator.swagger.io/api/gen/clients' // +'/typescript-node' post


/**
 * swagger 2.0 YAML 转 oas3 YAML
*/
export const convertUrl = 'https://converter.swagger.io/api/convert' // post



/**
 * 获取oas3 对应可生成的 client 类型
 */
export const oas3ClientsUrl = 'https://generator3.swagger.io/api/clients' // get

/**
 * oas3 生成 client
 */
export const oas3GenUrl = 'https://generator3.swagger.io/api/generate' // post

