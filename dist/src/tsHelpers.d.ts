export interface GenOptions {
    url: string;
    client_type: string;
    output_dir: string;
    convert?: boolean;
    clean?: boolean;
}
export type ApiData = {
    name: string;
    originJson: string;
    isOAS3: boolean;
};
export interface ReadApiDocsRes extends GenOptions {
    apiDatas: Array<ApiData | undefined>;
}
export type ApiDoc = ApiData & {
    lang: string;
    spec: string;
    type: 'CLIENT' | 'SERVER';
};
export interface ConvertRes extends ReadApiDocsRes {
    apiDocs: Array<ApiDoc | undefined>;
}
export interface DownloadFileRes extends ConvertRes {
    filePaths: Array<string | undefined>;
}
