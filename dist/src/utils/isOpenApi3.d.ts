/**
 * OAS3是指OpenAPI Specification 3.0，也称为Swagger 3.0。它是一种用于描述和定义RESTful API的规范。OpenAPI Specification（OAS）是一个开放标准，旨在提供一种统一的方式来描述API的结构、功能和操作
 */
export declare function isOpenApi3(doc: any): boolean;
