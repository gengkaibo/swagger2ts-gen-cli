export type UnaryFunction<ValueType, ReturnType> = (value: ValueType) => ReturnType | PromiseLike<ReturnType>;
export type Pipeline<ValueType, ReturnType> = (value?: ValueType) => Promise<ReturnType>;
/**
将promise返回和异步函数组合到一个可重用的管道中
@param ...input - Iterated over sequentially when returned `function` is called.
@returns The `input` functions are applied from left to right.
@example
```
const addUnicorn = async string => `${string} Unicorn`;
const addRainbow = async string => `${string} Rainbow`;
const pipeline = pipe(addUnicorn, addRainbow);
console.log(await pipeline('❤️'));
//=> '❤️ Unicorn Rainbow'
```
*/
export declare function pipe<ValueType, ReturnType>(f1: UnaryFunction<ValueType, ReturnType>): Pipeline<ValueType, ReturnType>;
export declare function pipe<ValueType, ResultValue1, ReturnType>(f1: UnaryFunction<ValueType, ResultValue1>, f2: UnaryFunction<ResultValue1, ReturnType>): Pipeline<ValueType, ReturnType>;
export declare function pipe<ValueType, ResultValue1, ResultValue2, ReturnType>(f1: UnaryFunction<ValueType, ResultValue1>, f2: UnaryFunction<ResultValue1, ResultValue2>, f3: UnaryFunction<ResultValue2, ReturnType>): Pipeline<ValueType, ReturnType>;
export declare function pipe<ValueType, ResultValue1, ResultValue2, ResultValue3, ReturnType>(f1: UnaryFunction<ValueType, ResultValue1>, f2: UnaryFunction<ResultValue1, ResultValue2>, f3: UnaryFunction<ResultValue2, ResultValue3>, f4: UnaryFunction<ResultValue3, ReturnType>): Pipeline<ValueType, ReturnType>;
export declare function pipe<ValueType, ResultValue1, ResultValue2, ResultValue3, ResultValue4, ReturnType>(f1: UnaryFunction<ValueType, ResultValue1>, f2: UnaryFunction<ResultValue1, ResultValue2>, f3: UnaryFunction<ResultValue2, ResultValue3>, f4: UnaryFunction<ResultValue3, ResultValue4>, f5: UnaryFunction<ResultValue4, ReturnType>): Pipeline<ValueType, ReturnType>;
export declare function pipe<ValueType, ResultValue1, ResultValue2, ResultValue3, ResultValue4, ResultValue5, ReturnType>(f1: UnaryFunction<ValueType, ResultValue1>, f2: UnaryFunction<ResultValue1, ResultValue2>, f3: UnaryFunction<ResultValue2, ResultValue3>, f4: UnaryFunction<ResultValue3, ResultValue4>, f5: UnaryFunction<ResultValue4, ResultValue5>, f6: UnaryFunction<ResultValue5, ReturnType>): Pipeline<ValueType, ReturnType>;
export declare function pipe<ValueType, ResultValue1, ResultValue2, ResultValue3, ResultValue4, ResultValue5, ResultValue6, ReturnType>(f1: UnaryFunction<ValueType, ResultValue1>, f2: UnaryFunction<ResultValue1, ResultValue2>, f3: UnaryFunction<ResultValue2, ResultValue3>, f4: UnaryFunction<ResultValue3, ResultValue4>, f5: UnaryFunction<ResultValue4, ResultValue5>, f6: UnaryFunction<ResultValue5, ResultValue6>, f7: UnaryFunction<ResultValue6, ReturnType>): Pipeline<ValueType, ReturnType>;
export declare function pipe<ValueType, ResultValue1, ResultValue2, ResultValue3, ResultValue4, ResultValue5, ResultValue6, ResultValue7, ReturnType>(f1: UnaryFunction<ValueType, ResultValue1>, f2: UnaryFunction<ResultValue1, ResultValue2>, f3: UnaryFunction<ResultValue2, ResultValue3>, f4: UnaryFunction<ResultValue3, ResultValue4>, f5: UnaryFunction<ResultValue4, ResultValue5>, f6: UnaryFunction<ResultValue5, ResultValue6>, f7: UnaryFunction<ResultValue6, ResultValue7>, f8: UnaryFunction<ResultValue7, ReturnType>): Pipeline<ValueType, ReturnType>;
export declare function pipe<ValueType, ResultValue1, ResultValue2, ResultValue3, ResultValue4, ResultValue5, ResultValue6, ResultValue7, ResultValue8, ReturnType>(f1: UnaryFunction<ValueType, ResultValue1>, f2: UnaryFunction<ResultValue1, ResultValue2>, f3: UnaryFunction<ResultValue2, ResultValue3>, f4: UnaryFunction<ResultValue3, ResultValue4>, f5: UnaryFunction<ResultValue4, ResultValue5>, f6: UnaryFunction<ResultValue5, ResultValue6>, f7: UnaryFunction<ResultValue6, ResultValue7>, f8: UnaryFunction<ResultValue7, ResultValue8>, f9: UnaryFunction<ResultValue8, ReturnType>): Pipeline<ValueType, ReturnType>;
