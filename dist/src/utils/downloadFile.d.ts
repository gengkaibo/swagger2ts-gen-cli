import { AxiosResponse } from 'axios';
export declare const downloadFile: (result: AxiosResponse, outputDir?: string) => Promise<string>;
