import { ConvertRes, DownloadFileRes } from '@/tsHelpers';
export declare const D_downloadFile: (lastData: ConvertRes) => Promise<DownloadFileRes>;
