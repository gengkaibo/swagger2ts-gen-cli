import { GenOptions, ReadApiDocsRes } from '@/tsHelpers';
export declare const B_readApiDocs: (options: GenOptions) => Promise<ReadApiDocsRes>;
