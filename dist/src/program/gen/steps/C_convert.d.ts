import { ConvertRes, ReadApiDocsRes } from '@/tsHelpers';
export declare const C_convert: (lastData: ReadApiDocsRes) => Promise<ConvertRes>;
