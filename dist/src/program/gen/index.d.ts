import { Command } from 'commander';
import { GenOptions } from '@/tsHelpers';
export declare const gen: (options: GenOptions) => Promise<void>;
export declare const genProgram: (program: Command) => void;
