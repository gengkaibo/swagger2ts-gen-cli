/**
 * 获取swagger 2.0 对应可生成的 client 类型
 */
export declare const swagger2ClientsUrl = "https://generator.swagger.io/api/gen/clients";
/**
 * swagger 2.0 生成 client
*/
export declare const swagger2GenUrl = "https://generator.swagger.io/api/gen/clients";
/**
 * swagger 2.0 YAML 转 oas3 YAML
*/
export declare const convertUrl = "https://converter.swagger.io/api/convert";
/**
 * 获取oas3 对应可生成的 client 类型
 */
export declare const oas3ClientsUrl = "https://generator3.swagger.io/api/clients";
/**
 * oas3 生成 client
 */
export declare const oas3GenUrl = "https://generator3.swagger.io/api/generate";
