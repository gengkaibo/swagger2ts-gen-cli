/* eslint-disable @typescript-eslint/no-var-requires */
/*
'off' or 0 - turn the rule off
'warn' or 1 - turn the rule on as a warning (doesn't affect exit code)
'error' or 2 - turn the rule on as an error (exit code is 1 when triggered)
*/
const { defineConfig } = require('eslint-define-config')
module.exports = defineConfig({
  root: true,
  extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 'latest',
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
    },
  },
  env: {
    node: true,
    es6: true,
  },
  rules: {
    quotes: ['error', 'single'], // 采用单引号
    semi: ['error', 'never'], // 取消末尾分号
    '@typescript-eslint/no-explicit-any': 'off', // 可以使用any
    'no-async-promise-executor': 'off', // 可以在Promise使用await
  },
})
