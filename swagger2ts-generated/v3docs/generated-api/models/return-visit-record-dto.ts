/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import {
    
} from ".";

/**
 * 
 *
 * @export
 * @interface ReturnVisitRecordDto
 */
export interface ReturnVisitRecordDto {

    /**
     * 主键
     *
     * @type {string}
     * @memberof ReturnVisitRecordDto
     */
    id?: string;

    /**
     * 订单编号
     *
     * @type {string}
     * @memberof ReturnVisitRecordDto
     */
    orderNo?: string;

    /**
     * 回访时间
     *
     * @type {Date}
     * @memberof ReturnVisitRecordDto
     */
    returnVisitTime?: Date;

    /**
     * 回访内容
     *
     * @type {string}
     * @memberof ReturnVisitRecordDto
     */
    returnVisitContent?: string;

    /**
     * 对接部门
     *
     * @type {string}
     * @memberof ReturnVisitRecordDto
     */
    department?: string;

    /**
     * 联系人
     *
     * @type {string}
     * @memberof ReturnVisitRecordDto
     */
    contactPerson?: string;

    /**
     * 联系事宜
     *
     * @type {string}
     * @memberof ReturnVisitRecordDto
     */
    contactMatter?: string;

    /**
     * 备注
     *
     * @type {string}
     * @memberof ReturnVisitRecordDto
     */
    remark?: string;

    /**
     * 客户id
     *
     * @type {string}
     * @memberof ReturnVisitRecordDto
     */
    returnVisitUserId?: string;

    /**
     * 线索id
     *
     * @type {string}
     * @memberof ReturnVisitRecordDto
     */
    clueId?: string;

    /**
     * 回访记录类型:0->订单回访记录;1->用户回访记录
     *
     * @type {number}
     * @memberof ReturnVisitRecordDto
     */
    returnVisitRecordType?: number;
}
