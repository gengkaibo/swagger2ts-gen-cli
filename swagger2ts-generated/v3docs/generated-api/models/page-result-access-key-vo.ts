/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { AccessKeyVo } from './access-key-vo';
import {
    AccessKeyVo,
} from ".";

/**
 * 
 *
 * @export
 * @interface PageResultAccessKeyVo
 */
export interface PageResultAccessKeyVo {

    /**
     * @type {number}
     * @memberof PageResultAccessKeyVo
     */
    pageNo?: number;

    /**
     * @type {number}
     * @memberof PageResultAccessKeyVo
     */
    pageSize?: number;

    /**
     * @type {number}
     * @memberof PageResultAccessKeyVo
     */
    total?: number;

    /**
     * @type {Array<AccessKeyVo>}
     * @memberof PageResultAccessKeyVo
     */
    list?: Array<AccessKeyVo>;
}
