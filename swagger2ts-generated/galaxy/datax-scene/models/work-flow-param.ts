/* tslint:disable */
/* eslint-disable */
/**
 * API测试文档
 * 接口测试文档
 *
 * OpenAPI spec version: v1.0
 * Contact: 310857864@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

 /**
 * 
 *
 * @export
 * @interface WorkFlowParam
 */
export interface WorkFlowParam {

    /**
     * 业务主键id
     *
     * @type {string}
     * @memberof WorkFlowParam
     */
    bussiId?: string;

    /**
     * 业务流程模型id
     *
     * @type {string}
     * @memberof WorkFlowParam
     */
    bussiModelId?: string;

    /**
     * 流程发起人姓名
     *
     * @type {string}
     * @memberof WorkFlowParam
     */
    createName?: string;

    /**
     * 第几页
     *
     * @type {number}
     * @memberof WorkFlowParam
     */
    pageNum?: number;

    /**
     * 页面条数
     *
     * @type {number}
     * @memberof WorkFlowParam
     */
    pageSize?: number;

    /**
     * 流程实例id
     *
     * @type {string}
     * @memberof WorkFlowParam
     */
    proInstId?: string;

    /**
     * 任务id
     *
     * @type {string}
     * @memberof WorkFlowParam
     */
    taskId?: string;

    /**
     * 流程发起人
     *
     * @type {string}
     * @memberof WorkFlowParam
     */
    userId?: string;
}
