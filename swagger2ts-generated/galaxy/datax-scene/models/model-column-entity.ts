/* tslint:disable */
/* eslint-disable */
/**
 * API测试文档
 * 接口测试文档
 *
 * OpenAPI spec version: v1.0
 * Contact: 310857864@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

 /**
 * 
 *
 * @export
 * @interface ModelColumnEntity
 */
export interface ModelColumnEntity {

    /**
     * 绑定数据标准字典字段
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    bindDictColumn?: string;

    /**
     * 绑定数据标准类别
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    bindDictTypeId?: string;

    /**
     * 列描述
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    columnComment?: string;

    /**
     * 列长度
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    columnLength?: string;

    /**
     * 列名称
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    columnName?: string;

    /**
     * 列小数位数
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    columnScale?: string;

    /**
     * 列类型
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    columnType?: string;

    /**
     * 列默认值
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    defaultValue?: string;

    /**
     * 显示类型（input文本框、textarea文本域、select下拉框、checkbox复选框、radio单选框、datetime日期控件）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    htmlType?: string;

    /**
     * 主键ID
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    id?: string;

    /**
     * 是否绑定数据标准（0否，1是）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    isBindDict?: string;

    /**
     * 是否详情字段（0否，1是）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    isDetail?: string;

    /**
     * 是否编辑字段（0否，1是）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    isEdit?: string;

    /**
     * 是否为插入字段（0否，1是）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    isInsert?: string;

    /**
     * 是否列表字段（0否，1是）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    isList?: string;

    /**
     * 是否主键（0否，1是）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    isPk?: string;

    /**
     * 是否查询字段（0否，1是）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    isQuery?: string;

    /**
     * 是否必填（0否，1是）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    isRequired?: string;

    /**
     * 是否系统默认（0否，1是）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    isSystem?: string;

    /**
     * 主数据模型Id
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    modelId?: string;

    /**
     * 查询方式（EQ等于、NE不等于、GT大于、GE大于等于、LT小于、LE小于等于、LIKE模糊、BETWEEN范围）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    queryType?: string;

    /**
     * 备注
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    remark?: string;

    /**
     * 排序
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    sort?: string;

    /**
     * 状态（0不启用，1启用）
     *
     * @type {string}
     * @memberof ModelColumnEntity
     */
    status?: string;
}
