/* tslint:disable */
/* eslint-disable */
/**
 * 系统模块接口文档
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { SysDept } from './sys-dept';
import { SysRoleRes } from './sys-role-res';
 /**
 * 
 *
 * @export
 * @interface SysUserRes
 */
export interface SysUserRes {

    /**
     * @type {boolean}
     * @memberof SysUserRes
     */
    admin?: boolean;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    avatar?: string;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    createBy?: string;

    /**
     * @type {Date}
     * @memberof SysUserRes
     */
    createTime?: Date;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    delFlag?: string;

    /**
     * @type {SysDept}
     * @memberof SysUserRes
     */
    dept?: SysDept;

    /**
     * @type {number}
     * @memberof SysUserRes
     */
    deptId?: number;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    email?: string;

    /**
     * @type {Date}
     * @memberof SysUserRes
     */
    loginDate?: Date;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    loginIp?: string;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    nickName?: string;

    /**
     * @type {any}
     * @memberof SysUserRes
     */
    params?: any;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    password?: string;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    phonenumber?: string;

    /**
     * @type {Array<number>}
     * @memberof SysUserRes
     */
    postIds?: Array<number>;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    remark?: string;

    /**
     * @type {number}
     * @memberof SysUserRes
     */
    roleId?: number;

    /**
     * @type {Array<number>}
     * @memberof SysUserRes
     */
    roleIds?: Array<number>;

    /**
     * @type {Array<SysRoleRes>}
     * @memberof SysUserRes
     */
    roles?: Array<SysRoleRes>;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    sex?: string;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    status?: string;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    updateBy?: string;

    /**
     * @type {Date}
     * @memberof SysUserRes
     */
    updateTime?: Date;

    /**
     * @type {number}
     * @memberof SysUserRes
     */
    userId?: number;

    /**
     * @type {string}
     * @memberof SysUserRes
     */
    userName?: string;
}
