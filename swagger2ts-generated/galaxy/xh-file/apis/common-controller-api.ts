/* tslint:disable */
/* eslint-disable */
/**
 * 
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import globalAxios, { AxiosResponse, AxiosInstance, AxiosRequestConfig } from 'axios';
import { Configuration } from '../configuration';
// Some imports not used depending on template conditions
// @ts-ignore
import { BASE_PATH, COLLECTION_FORMATS, RequestArgs, BaseAPI, RequiredError } from '../base';
/**
 * CommonControllerApi - axios parameter creator
 * @export
 */
export const CommonControllerApiAxiosParamCreator = function (configuration?: Configuration) {
    return {
        /**
         * 
         * @summary fileDownload
         * @param {boolean} [_delete] delete
         * @param {string} [fileName] fileName
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        fileDownloadUsingGET: async (_delete?: boolean, fileName?: string, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/common/download`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions :AxiosRequestConfig = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Authorization required
            if (configuration && configuration.apiKey) {
                const localVarApiKeyValue = typeof configuration.apiKey === 'function'
                    ? await configuration.apiKey("Authorization")
                    : await configuration.apiKey;
                localVarHeaderParameter["Authorization"] = localVarApiKeyValue;
            }

            if (_delete !== undefined) {
                localVarQueryParameter['delete'] = _delete;
            }

            if (fileName !== undefined) {
                localVarQueryParameter['fileName'] = fileName;
            }

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.params) {
                query.set(key, options.params[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary resourceDownload
         * @param {string} [resource] resource
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        resourceDownloadUsingGET: async (resource?: string, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/common/download/resource`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions :AxiosRequestConfig = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Authorization required
            if (configuration && configuration.apiKey) {
                const localVarApiKeyValue = typeof configuration.apiKey === 'function'
                    ? await configuration.apiKey("Authorization")
                    : await configuration.apiKey;
                localVarHeaderParameter["Authorization"] = localVarApiKeyValue;
            }

            if (resource !== undefined) {
                localVarQueryParameter['resource'] = resource;
            }

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.params) {
                query.set(key, options.params[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary uploadFile
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        uploadFileUsingPOST: async (options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/common/upload`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions :AxiosRequestConfig = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Authorization required
            if (configuration && configuration.apiKey) {
                const localVarApiKeyValue = typeof configuration.apiKey === 'function'
                    ? await configuration.apiKey("Authorization")
                    : await configuration.apiKey;
                localVarHeaderParameter["Authorization"] = localVarApiKeyValue;
            }

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.params) {
                query.set(key, options.params[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * 
         * @summary uploadFiles
         * @param {Array<any>} [body] files
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        uploadFilesUsingPOST: async (body?: Array<any>, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/common/uploads`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions :AxiosRequestConfig = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            // authentication Authorization required
            if (configuration && configuration.apiKey) {
                const localVarApiKeyValue = typeof configuration.apiKey === 'function'
                    ? await configuration.apiKey("Authorization")
                    : await configuration.apiKey;
                localVarHeaderParameter["Authorization"] = localVarApiKeyValue;
            }

            localVarHeaderParameter['Content-Type'] = 'application/json';

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.params) {
                query.set(key, options.params[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            const needsSerialization = (typeof body !== "string") || localVarRequestOptions.headers['Content-Type'] === 'application/json';
            localVarRequestOptions.data =  needsSerialization ? JSON.stringify(body !== undefined ? body : {}) : (body || "");

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
    }
};

/**
 * CommonControllerApi - functional programming interface
 * @export
 */
export const CommonControllerApiFp = function(configuration?: Configuration) {
    return {
        /**
         * 
         * @summary fileDownload
         * @param {boolean} [_delete] delete
         * @param {string} [fileName] fileName
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async fileDownloadUsingGET(_delete?: boolean, fileName?: string, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => Promise<AxiosResponse<void>>> {
            const localVarAxiosArgs = await CommonControllerApiAxiosParamCreator(configuration).fileDownloadUsingGET(_delete, fileName, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs :AxiosRequestConfig = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * 
         * @summary resourceDownload
         * @param {string} [resource] resource
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async resourceDownloadUsingGET(resource?: string, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => Promise<AxiosResponse<void>>> {
            const localVarAxiosArgs = await CommonControllerApiAxiosParamCreator(configuration).resourceDownloadUsingGET(resource, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs :AxiosRequestConfig = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * 
         * @summary uploadFile
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async uploadFileUsingPOST(options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => Promise<AxiosResponse<{ [key: string]: any; }>>> {
            const localVarAxiosArgs = await CommonControllerApiAxiosParamCreator(configuration).uploadFileUsingPOST(options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs :AxiosRequestConfig = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * 
         * @summary uploadFiles
         * @param {Array<any>} [body] files
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async uploadFilesUsingPOST(body?: Array<any>, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => Promise<AxiosResponse<{ [key: string]: any; }>>> {
            const localVarAxiosArgs = await CommonControllerApiAxiosParamCreator(configuration).uploadFilesUsingPOST(body, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs :AxiosRequestConfig = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
    }
};

/**
 * CommonControllerApi - factory interface
 * @export
 */
export const CommonControllerApiFactory = function (configuration?: Configuration, basePath?: string, axios?: AxiosInstance) {
    return {
        /**
         * 
         * @summary fileDownload
         * @param {boolean} [_delete] delete
         * @param {string} [fileName] fileName
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async fileDownloadUsingGET(_delete?: boolean, fileName?: string, options?: AxiosRequestConfig): Promise<AxiosResponse<void>> {
            return CommonControllerApiFp(configuration).fileDownloadUsingGET(_delete, fileName, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary resourceDownload
         * @param {string} [resource] resource
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async resourceDownloadUsingGET(resource?: string, options?: AxiosRequestConfig): Promise<AxiosResponse<void>> {
            return CommonControllerApiFp(configuration).resourceDownloadUsingGET(resource, options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary uploadFile
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async uploadFileUsingPOST(options?: AxiosRequestConfig): Promise<AxiosResponse<{ [key: string]: any; }>> {
            return CommonControllerApiFp(configuration).uploadFileUsingPOST(options).then((request) => request(axios, basePath));
        },
        /**
         * 
         * @summary uploadFiles
         * @param {Array<any>} [body] files
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async uploadFilesUsingPOST(body?: Array<any>, options?: AxiosRequestConfig): Promise<AxiosResponse<{ [key: string]: any; }>> {
            return CommonControllerApiFp(configuration).uploadFilesUsingPOST(body, options).then((request) => request(axios, basePath));
        },
    };
};

/**
 * CommonControllerApi - object-oriented interface
 * @export
 * @class CommonControllerApi
 * @extends {BaseAPI}
 */
export class CommonControllerApi extends BaseAPI {
    /**
     * 
     * @summary fileDownload
     * @param {boolean} [_delete] delete
     * @param {string} [fileName] fileName
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof CommonControllerApi
     */
    public async fileDownloadUsingGET(_delete?: boolean, fileName?: string, options?: AxiosRequestConfig) : Promise<AxiosResponse<void>> {
        return CommonControllerApiFp(this.configuration).fileDownloadUsingGET(_delete, fileName, options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * 
     * @summary resourceDownload
     * @param {string} [resource] resource
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof CommonControllerApi
     */
    public async resourceDownloadUsingGET(resource?: string, options?: AxiosRequestConfig) : Promise<AxiosResponse<void>> {
        return CommonControllerApiFp(this.configuration).resourceDownloadUsingGET(resource, options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * 
     * @summary uploadFile
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof CommonControllerApi
     */
    public async uploadFileUsingPOST(options?: AxiosRequestConfig) : Promise<AxiosResponse<{ [key: string]: any; }>> {
        return CommonControllerApiFp(this.configuration).uploadFileUsingPOST(options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * 
     * @summary uploadFiles
     * @param {Array<any>} [body] files
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof CommonControllerApi
     */
    public async uploadFilesUsingPOST(body?: Array<any>, options?: AxiosRequestConfig) : Promise<AxiosResponse<{ [key: string]: any; }>> {
        return CommonControllerApiFp(this.configuration).uploadFilesUsingPOST(body, options).then((request) => request(this.axios, this.basePath));
    }
}
