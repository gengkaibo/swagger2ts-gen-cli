/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { ReturnVisitRecordVo } from './return-visit-record-vo';
/**
 * 
 * @export
 * @interface ResultListReturnVisitRecordVo
 */
export interface ResultListReturnVisitRecordVo {
    /**
     * 
     * @type {number}
     * @memberof ResultListReturnVisitRecordVo
     */
    code?: number;
    /**
     * 
     * @type {string}
     * @memberof ResultListReturnVisitRecordVo
     */
    message?: string;
    /**
     * 
     * @type {Array<ReturnVisitRecordVo>}
     * @memberof ResultListReturnVisitRecordVo
     */
    data?: Array<ReturnVisitRecordVo>;
    /**
     * 
     * @type {boolean}
     * @memberof ResultListReturnVisitRecordVo
     */
    success?: boolean;
}
