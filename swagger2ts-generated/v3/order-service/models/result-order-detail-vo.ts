/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { OrderDetailVo } from './order-detail-vo';
/**
 * 
 * @export
 * @interface ResultOrderDetailVo
 */
export interface ResultOrderDetailVo {
    /**
     * 
     * @type {number}
     * @memberof ResultOrderDetailVo
     */
    code?: number;
    /**
     * 
     * @type {string}
     * @memberof ResultOrderDetailVo
     */
    message?: string;
    /**
     * 
     * @type {OrderDetailVo}
     * @memberof ResultOrderDetailVo
     */
    data?: OrderDetailVo;
    /**
     * 
     * @type {boolean}
     * @memberof ResultOrderDetailVo
     */
    success?: boolean;
}
