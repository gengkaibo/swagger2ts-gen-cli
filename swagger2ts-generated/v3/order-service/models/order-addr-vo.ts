/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 
 * @export
 * @interface OrderAddrVo
 */
export interface OrderAddrVo {
    /**
     * 主键
     * @type {string}
     * @memberof OrderAddrVo
     */
    id?: string;
    /**
     * 省级编码
     * @type {number}
     * @memberof OrderAddrVo
     */
    provinceCode?: number;
    /**
     * 省
     * @type {string}
     * @memberof OrderAddrVo
     */
    province?: string;
    /**
     * 市级编码
     * @type {number}
     * @memberof OrderAddrVo
     */
    cityCode?: number;
    /**
     * 市
     * @type {string}
     * @memberof OrderAddrVo
     */
    city?: string;
    /**
     * 县(区)编码
     * @type {number}
     * @memberof OrderAddrVo
     */
    areaCode?: number;
    /**
     * 县(区)
     * @type {string}
     * @memberof OrderAddrVo
     */
    area?: string;
    /**
     * 详细地址
     * @type {string}
     * @memberof OrderAddrVo
     */
    addr?: string;
}
