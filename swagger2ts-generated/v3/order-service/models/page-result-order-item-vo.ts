/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { OrderItemVo } from './order-item-vo';
/**
 * 
 * @export
 * @interface PageResultOrderItemVo
 */
export interface PageResultOrderItemVo {
    /**
     * 
     * @type {number}
     * @memberof PageResultOrderItemVo
     */
    pageNo?: number;
    /**
     * 
     * @type {number}
     * @memberof PageResultOrderItemVo
     */
    pageSize?: number;
    /**
     * 
     * @type {number}
     * @memberof PageResultOrderItemVo
     */
    total?: number;
    /**
     * 
     * @type {Array<OrderItemVo>}
     * @memberof PageResultOrderItemVo
     */
    list?: Array<OrderItemVo>;
}
