/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { PageResultOrderDeliveryVo } from './page-result-order-delivery-vo';
/**
 * 
 * @export
 * @interface ResultPageResultOrderDeliveryVo
 */
export interface ResultPageResultOrderDeliveryVo {
    /**
     * 
     * @type {number}
     * @memberof ResultPageResultOrderDeliveryVo
     */
    code?: number;
    /**
     * 
     * @type {string}
     * @memberof ResultPageResultOrderDeliveryVo
     */
    message?: string;
    /**
     * 
     * @type {PageResultOrderDeliveryVo}
     * @memberof ResultPageResultOrderDeliveryVo
     */
    data?: PageResultOrderDeliveryVo;
    /**
     * 
     * @type {boolean}
     * @memberof ResultPageResultOrderDeliveryVo
     */
    success?: boolean;
}
