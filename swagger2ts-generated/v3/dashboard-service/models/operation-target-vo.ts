/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 
 * @export
 * @interface OperationTargetVo
 */
export interface OperationTargetVo {
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    weekUserCount?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    monthUserCount?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    weekOrderCount?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    monthOrderCount?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    weekVisitCount?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    monthVisitCount?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    pv?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    uv?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    vv?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    ip?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    dau?: number;
    /**
     * 
     * @type {number}
     * @memberof OperationTargetVo
     */
    mau?: number;
}
