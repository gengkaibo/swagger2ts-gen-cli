/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 
 * @export
 * @interface OrderStatisticsVo
 */
export interface OrderStatisticsVo {
    /**
     * 订单总量
     * @type {number}
     * @memberof OrderStatisticsVo
     */
    totalCount?: number;
    /**
     * 营收
     * @type {number}
     * @memberof OrderStatisticsVo
     */
    totalAmount?: number;
    /**
     * 待审核订单
     * @type {number}
     * @memberof OrderStatisticsVo
     */
    toBeAudit?: number;
    /**
     * 审核不通过订单
     * @type {number}
     * @memberof OrderStatisticsVo
     */
    auditFailed?: number;
    /**
     * 执行中订单
     * @type {number}
     * @memberof OrderStatisticsVo
     */
    inExecution?: number;
    /**
     * 已完成订单
     * @type {number}
     * @memberof OrderStatisticsVo
     */
    completed?: number;
}
