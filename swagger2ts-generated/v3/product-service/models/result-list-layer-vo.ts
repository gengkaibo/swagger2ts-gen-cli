/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { LayerVo } from './layer-vo';
/**
 * 
 * @export
 * @interface ResultListLayerVo
 */
export interface ResultListLayerVo {
    /**
     * 
     * @type {number}
     * @memberof ResultListLayerVo
     */
    code?: number;
    /**
     * 
     * @type {string}
     * @memberof ResultListLayerVo
     */
    message?: string;
    /**
     * 
     * @type {Array<LayerVo>}
     * @memberof ResultListLayerVo
     */
    data?: Array<LayerVo>;
    /**
     * 
     * @type {boolean}
     * @memberof ResultListLayerVo
     */
    success?: boolean;
}
