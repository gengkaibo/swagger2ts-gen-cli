/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { SphereVo } from './sphere-vo';
/**
 * 
 * @export
 * @interface PageResultSphereVo
 */
export interface PageResultSphereVo {
    /**
     * 
     * @type {number}
     * @memberof PageResultSphereVo
     */
    pageNo?: number;
    /**
     * 
     * @type {number}
     * @memberof PageResultSphereVo
     */
    pageSize?: number;
    /**
     * 
     * @type {number}
     * @memberof PageResultSphereVo
     */
    total?: number;
    /**
     * 
     * @type {Array<SphereVo>}
     * @memberof PageResultSphereVo
     */
    list?: Array<SphereVo>;
}
