/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 
 * @export
 * @interface SphereCatalogDto
 */
export interface SphereCatalogDto {
    /**
     * 主键
     * @type {string}
     * @memberof SphereCatalogDto
     */
    id?: string;
    /**
     * 三维球id
     * @type {string}
     * @memberof SphereCatalogDto
     */
    sphereId: string;
    /**
     * 节点名称
     * @type {string}
     * @memberof SphereCatalogDto
     */
    nodeName?: string;
    /**
     * 专题类型
     * @type {string}
     * @memberof SphereCatalogDto
     */
    themeType?: string;
    /**
     * 专题类型名称
     * @type {string}
     * @memberof SphereCatalogDto
     */
    themeTypeName?: string;
    /**
     * 图标
     * @type {string}
     * @memberof SphereCatalogDto
     */
    icon?: string;
    /**
     * 节点类型
     * @type {number}
     * @memberof SphereCatalogDto
     */
    nodeType?: number;
    /**
     * 前驱节点id
     * @type {string}
     * @memberof SphereCatalogDto
     */
    prevId?: string;
    /**
     * 后继节点id
     * @type {string}
     * @memberof SphereCatalogDto
     */
    nextId?: string;
    /**
     * 父节点id
     * @type {string}
     * @memberof SphereCatalogDto
     */
    parentId?: string;
    /**
     * 节点级别
     * @type {number}
     * @memberof SphereCatalogDto
     */
    level?: number;
    /**
     * 目标节点id
     * @type {string}
     * @memberof SphereCatalogDto
     */
    targetNodeId?: string;
    /**
     * 操作类型:before;after;inner;insert;edit;delete
     * @type {string}
     * @memberof SphereCatalogDto
     */
    action: string;
}
