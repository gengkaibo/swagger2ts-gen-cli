/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import globalAxios, { AxiosResponse, AxiosInstance, AxiosRequestConfig } from 'axios';
import { Configuration } from '../configuration';
// Some imports not used depending on template conditions
// @ts-ignore
import { BASE_PATH, COLLECTION_FORMATS, RequestArgs, BaseAPI, RequiredError } from '../base';
import { OssUploadBody } from '../models';
import { ResultListAreaVo } from '../models';
import { ResultString } from '../models';
/**
 * App_Api - axios parameter creator
 * @export
 */
export const App_ApiAxiosParamCreator = function (configuration?: Configuration) {
    return {
        /**
         * 根据父级编码获取子级行政区划
         * @summary 获取子级行政区划
         * @param {number} parentCode 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        listByParentCode: async (parentCode: number, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            // verify required parameter 'parentCode' is not null or undefined
            if (parentCode === null || parentCode === undefined) {
                throw new RequiredError('parentCode','Required parameter parentCode was null or undefined when calling listByParentCode.');
            }
            const localVarPath = `/app/area/listByParentCode/{parentCode}`
                .replace(`{${"parentCode"}}`, encodeURIComponent(String(parentCode)));
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions :AxiosRequestConfig = { method: 'GET', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.params) {
                query.set(key, options.params[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
        /**
         * 文件上传接口
         * @summary 文件上传
         * @param {OssUploadBody} [body] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        upload: async (body?: OssUploadBody, options: AxiosRequestConfig = {}): Promise<RequestArgs> => {
            const localVarPath = `/app/oss/upload`;
            // use dummy base URL string because the URL constructor only accepts absolute URLs.
            const localVarUrlObj = new URL(localVarPath, 'https://example.com');
            let baseOptions;
            if (configuration) {
                baseOptions = configuration.baseOptions;
            }
            const localVarRequestOptions :AxiosRequestConfig = { method: 'POST', ...baseOptions, ...options};
            const localVarHeaderParameter = {} as any;
            const localVarQueryParameter = {} as any;

            localVarHeaderParameter['Content-Type'] = 'application/json';

            const query = new URLSearchParams(localVarUrlObj.search);
            for (const key in localVarQueryParameter) {
                query.set(key, localVarQueryParameter[key]);
            }
            for (const key in options.params) {
                query.set(key, options.params[key]);
            }
            localVarUrlObj.search = (new URLSearchParams(query)).toString();
            let headersFromBaseOptions = baseOptions && baseOptions.headers ? baseOptions.headers : {};
            localVarRequestOptions.headers = {...localVarHeaderParameter, ...headersFromBaseOptions, ...options.headers};
            const needsSerialization = (typeof body !== "string") || localVarRequestOptions.headers['Content-Type'] === 'application/json';
            localVarRequestOptions.data =  needsSerialization ? JSON.stringify(body !== undefined ? body : {}) : (body || "");

            return {
                url: localVarUrlObj.pathname + localVarUrlObj.search + localVarUrlObj.hash,
                options: localVarRequestOptions,
            };
        },
    }
};

/**
 * App_Api - functional programming interface
 * @export
 */
export const App_ApiFp = function(configuration?: Configuration) {
    return {
        /**
         * 根据父级编码获取子级行政区划
         * @summary 获取子级行政区划
         * @param {number} parentCode 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async listByParentCode(parentCode: number, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => Promise<AxiosResponse<ResultListAreaVo>>> {
            const localVarAxiosArgs = await App_ApiAxiosParamCreator(configuration).listByParentCode(parentCode, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs :AxiosRequestConfig = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
        /**
         * 文件上传接口
         * @summary 文件上传
         * @param {OssUploadBody} [body] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async upload(body?: OssUploadBody, options?: AxiosRequestConfig): Promise<(axios?: AxiosInstance, basePath?: string) => Promise<AxiosResponse<ResultString>>> {
            const localVarAxiosArgs = await App_ApiAxiosParamCreator(configuration).upload(body, options);
            return (axios: AxiosInstance = globalAxios, basePath: string = BASE_PATH) => {
                const axiosRequestArgs :AxiosRequestConfig = {...localVarAxiosArgs.options, url: basePath + localVarAxiosArgs.url};
                return axios.request(axiosRequestArgs);
            };
        },
    }
};

/**
 * App_Api - factory interface
 * @export
 */
export const App_ApiFactory = function (configuration?: Configuration, basePath?: string, axios?: AxiosInstance) {
    return {
        /**
         * 根据父级编码获取子级行政区划
         * @summary 获取子级行政区划
         * @param {number} parentCode 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async listByParentCode(parentCode: number, options?: AxiosRequestConfig): Promise<AxiosResponse<ResultListAreaVo>> {
            return App_ApiFp(configuration).listByParentCode(parentCode, options).then((request) => request(axios, basePath));
        },
        /**
         * 文件上传接口
         * @summary 文件上传
         * @param {OssUploadBody} [body] 
         * @param {*} [options] Override http request option.
         * @throws {RequiredError}
         */
        async upload(body?: OssUploadBody, options?: AxiosRequestConfig): Promise<AxiosResponse<ResultString>> {
            return App_ApiFp(configuration).upload(body, options).then((request) => request(axios, basePath));
        },
    };
};

/**
 * App_Api - object-oriented interface
 * @export
 * @class App_Api
 * @extends {BaseAPI}
 */
export class App_Api extends BaseAPI {
    /**
     * 根据父级编码获取子级行政区划
     * @summary 获取子级行政区划
     * @param {number} parentCode 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof App_Api
     */
    public async listByParentCode(parentCode: number, options?: AxiosRequestConfig) : Promise<AxiosResponse<ResultListAreaVo>> {
        return App_ApiFp(this.configuration).listByParentCode(parentCode, options).then((request) => request(this.axios, this.basePath));
    }
    /**
     * 文件上传接口
     * @summary 文件上传
     * @param {OssUploadBody} [body] 
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof App_Api
     */
    public async upload(body?: OssUploadBody, options?: AxiosRequestConfig) : Promise<AxiosResponse<ResultString>> {
        return App_ApiFp(this.configuration).upload(body, options).then((request) => request(this.axios, this.basePath));
    }
}
