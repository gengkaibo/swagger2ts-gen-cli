/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 
 * @export
 * @interface AreaVo
 */
export interface AreaVo {
    /**
     * 主键
     * @type {string}
     * @memberof AreaVo
     */
    id?: string;
    /**
     * 行政区划编码
     * @type {number}
     * @memberof AreaVo
     */
    code?: number;
    /**
     * 父级行政区划编码
     * @type {number}
     * @memberof AreaVo
     */
    parentCode?: number;
    /**
     * 行政区名称
     * @type {string}
     * @memberof AreaVo
     */
    name?: string;
    /**
     * 行政区级别
     * @type {string}
     * @memberof AreaVo
     */
    level?: string;
}
