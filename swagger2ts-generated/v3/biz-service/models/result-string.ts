/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 
 * @export
 * @interface ResultString
 */
export interface ResultString {
    /**
     * 
     * @type {number}
     * @memberof ResultString
     */
    code?: number;
    /**
     * 
     * @type {string}
     * @memberof ResultString
     */
    message?: string;
    /**
     * 
     * @type {string}
     * @memberof ResultString
     */
    data?: string;
    /**
     * 
     * @type {boolean}
     * @memberof ResultString
     */
    success?: boolean;
}
