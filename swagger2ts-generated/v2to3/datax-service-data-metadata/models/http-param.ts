/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: 985134801@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import {
    
} from ".";

/**
 * 
 *
 * @export
 * @interface HttpParam
 */
export interface HttpParam {

    /**
     * @type {string}
     * @memberof HttpParam
     */
    key?: string;

    /**
     * @type {any}
     * @memberof HttpParam
     */
    value?: any;
}
