/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: 985134801@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { SQLException } from './sqlexception';
import { SQLWarning } from './sqlwarning';
import { StackTraceElement } from './stack-trace-element';
import { Throwable } from './throwable';
import {
    SQLException,SQLWarning,StackTraceElement,Throwable,
} from ".";

/**
 * 
 *
 * @export
 * @interface SQLWarning
 */
export interface SQLWarning {

    /**
     * @type {string}
     * @memberof SQLWarning
     */
    sQLState?: string;

    /**
     * @type {Throwable}
     * @memberof SQLWarning
     */
    cause?: Throwable;

    /**
     * @type {string}
     * @memberof SQLWarning
     */
    detailMessage?: string;

    /**
     * @type {number}
     * @memberof SQLWarning
     */
    errorCode?: number;

    /**
     * @type {string}
     * @memberof SQLWarning
     */
    localizedMessage?: string;

    /**
     * @type {string}
     * @memberof SQLWarning
     */
    message?: string;

    /**
     * @type {SQLException}
     * @memberof SQLWarning
     */
    next?: SQLException;

    /**
     * @type {SQLException}
     * @memberof SQLWarning
     */
    nextException?: SQLException;

    /**
     * @type {SQLWarning}
     * @memberof SQLWarning
     */
    nextWarning?: SQLWarning;

    /**
     * @type {Array<StackTraceElement>}
     * @memberof SQLWarning
     */
    ourStackTrace?: Array<StackTraceElement>;

    /**
     * @type {string}
     * @memberof SQLWarning
     */
    sqlstate?: string;

    /**
     * @type {Array<StackTraceElement>}
     * @memberof SQLWarning
     */
    stackTrace?: Array<StackTraceElement>;

    /**
     * @type {number}
     * @memberof SQLWarning
     */
    stackTraceDepth?: number;

    /**
     * @type {Array<Throwable>}
     * @memberof SQLWarning
     */
    suppressed?: Array<Throwable>;

    /**
     * @type {Array<Throwable>}
     * @memberof SQLWarning
     */
    suppressedExceptions?: Array<Throwable>;

    /**
     * @type {number}
     * @memberof SQLWarning
     */
    vendorCode?: number;
}
