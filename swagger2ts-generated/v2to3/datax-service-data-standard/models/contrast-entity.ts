/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: 985134801@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import {
    
} from ".";

/**
 * 
 *
 * @export
 * @interface ContrastEntity
 */
export interface ContrastEntity {

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    bindGbColumn?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    columnComment?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    columnId?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    columnName?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    createBy?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    createDept?: string;

    /**
     * @type {Date}
     * @memberof ContrastEntity
     */
    createTime?: Date;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    gbTypeCode?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    gbTypeId?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    gbTypeName?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    id?: string;

    /**
     * @type {number}
     * @memberof ContrastEntity
     */
    mappingCount?: number;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    remark?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    sourceId?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    sourceName?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    status?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    tableComment?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    tableId?: string;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    tableName?: string;

    /**
     * @type {number}
     * @memberof ContrastEntity
     */
    unMappingCount?: number;

    /**
     * @type {string}
     * @memberof ContrastEntity
     */
    updateBy?: string;

    /**
     * @type {Date}
     * @memberof ContrastEntity
     */
    updateTime?: Date;
}
