/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import {
    
} from ".";

/**
 * 
 *
 * @export
 * @interface Model
 */
export interface Model {

    /**
     * hdfs浏览路径
     *
     * @type {string}
     * @memberof Model
     */
    hdfsUrl?: string;

    /**
     * 数据表中文名称
     *
     * @type {string}
     * @memberof Model
     */
    tableComment?: string;

    /**
     * 数据表主键
     *
     * @type {string}
     * @memberof Model
     */
    tableId?: string;

    /**
     * 数据表名称
     *
     * @type {string}
     * @memberof Model
     */
    tableName?: string;
}
