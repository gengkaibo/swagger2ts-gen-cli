/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 
 * @export
 * @interface OutModelGroupDTO
 */
export interface OutModelGroupDTO {
    /**
     * 
     * @type {Date}
     * @memberof OutModelGroupDTO
     */
    createTime?: Date;
    /**
     * 
     * @type {string}
     * @memberof OutModelGroupDTO
     */
    createUserId?: string;
    /**
     * 
     * @type {string}
     * @memberof OutModelGroupDTO
     */
    describe?: string;
    /**
     * 
     * @type {string}
     * @memberof OutModelGroupDTO
     */
    enabled?: string;
    /**
     * 
     * @type {string}
     * @memberof OutModelGroupDTO
     */
    groupId?: string;
    /**
     * 
     * @type {string}
     * @memberof OutModelGroupDTO
     */
    groupName?: string;
    /**
     * 
     * @type {number}
     * @memberof OutModelGroupDTO
     */
    groupOrder?: number;
    /**
     * 
     * @type {string}
     * @memberof OutModelGroupDTO
     */
    groupType?: string;
    /**
     * 
     * @type {Date}
     * @memberof OutModelGroupDTO
     */
    updateTime?: Date;
}
