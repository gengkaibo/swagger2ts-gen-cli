/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { ModelFilterVO } from './model-filter-vo';
import { OutinterfaceBasicModelMetaDataCopyDTO } from './outinterface-basic-model-meta-data-copy-dto';
/**
 * 基础模型返回对象
 * @export
 * @interface UpdateOutinterfaceModelVO
 */
export interface UpdateOutinterfaceModelVO {
    /**
     * 基础模型组
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    basicModelGroupName?: string;
    /**
     * 基础模型管理元数据
     * @type {Array<OutinterfaceBasicModelMetaDataCopyDTO>}
     * @memberof UpdateOutinterfaceModelVO
     */
    basicModelMetaDataCopyDTOS?: Array<OutinterfaceBasicModelMetaDataCopyDTO>;
    /**
     * 基础模型
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    basicModelTableNameValue?: string;
    /**
     * 字段数量
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    columnCount?: string;
    /**
     * 创建时间
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    createTime?: string;
    /**
     * 创建人
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    createUserName?: string;
    /**
     * 备注
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    description?: string;
    /**
     * 是否有效
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    enabled?: string;
    /**
     * 所属分组
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    group?: string;
    /**
     * 预过滤条件
     * @type {Array<ModelFilterVO>}
     * @memberof UpdateOutinterfaceModelVO
     */
    modelFilterVOS?: Array<ModelFilterVO>;
    /**
     * 模型id
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    modelName?: string;
    /**
     * 名称
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    name?: string;
    /**
     * 排序
     * @type {string}
     * @memberof UpdateOutinterfaceModelVO
     */
    sortNum?: string;
}
