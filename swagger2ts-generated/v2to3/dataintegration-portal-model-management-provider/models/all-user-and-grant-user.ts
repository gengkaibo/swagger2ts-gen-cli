/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { UserListDTO } from './user-list-dto';
/**
 * 
 * @export
 * @interface AllUserAndGrantUser
 */
export interface AllUserAndGrantUser {
    /**
     * 
     * @type {Array<UserListDTO>}
     * @memberof AllUserAndGrantUser
     */
    allUser?: Array<UserListDTO>;
    /**
     * 
     * @type {Array<UserListDTO>}
     * @memberof AllUserAndGrantUser
     */
    grantUser?: Array<UserListDTO>;
}
