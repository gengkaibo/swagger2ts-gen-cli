/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { BasicModelDTO } from './basic-model-dto';
/**
 * 
 * @export
 * @interface PageInfoBasicModelDTO
 */
export interface PageInfoBasicModelDTO {
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    endRow?: number;
    /**
     * 
     * @type {boolean}
     * @memberof PageInfoBasicModelDTO
     */
    hasNextPage?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof PageInfoBasicModelDTO
     */
    hasPreviousPage?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof PageInfoBasicModelDTO
     */
    isFirstPage?: boolean;
    /**
     * 
     * @type {boolean}
     * @memberof PageInfoBasicModelDTO
     */
    isLastPage?: boolean;
    /**
     * 
     * @type {Array<BasicModelDTO>}
     * @memberof PageInfoBasicModelDTO
     */
    list?: Array<BasicModelDTO>;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    navigateFirstPage?: number;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    navigateLastPage?: number;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    navigatePages?: number;
    /**
     * 
     * @type {Array<number>}
     * @memberof PageInfoBasicModelDTO
     */
    navigatepageNums?: Array<number>;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    nextPage?: number;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    pageNum?: number;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    pageSize?: number;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    pages?: number;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    prePage?: number;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    size?: number;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    startRow?: number;
    /**
     * 
     * @type {number}
     * @memberof PageInfoBasicModelDTO
     */
    total?: number;
}
