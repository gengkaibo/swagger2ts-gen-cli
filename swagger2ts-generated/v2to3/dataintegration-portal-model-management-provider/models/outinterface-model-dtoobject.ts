/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { OutinterfaceModelDTO } from './outinterface-model-dto';
/**
 * 
 * @export
 * @interface OutinterfaceModelDTOobject
 */
export interface OutinterfaceModelDTOobject {
    /**
     * 附件（满足不同场景需求返回数据）
     * @type {any}
     * @memberof OutinterfaceModelDTOobject
     */
    attachment?: any;
    /**
     * 返回码
     * @type {string}
     * @memberof OutinterfaceModelDTOobject
     */
    code?: string;
    /**
     * 
     * @type {OutinterfaceModelDTO}
     * @memberof OutinterfaceModelDTOobject
     */
    content?: OutinterfaceModelDTO;
    /**
     * 返回描述信息
     * @type {string}
     * @memberof OutinterfaceModelDTOobject
     */
    msg?: string;
    /**
     * 返回内容的签名数据
     * @type {string}
     * @memberof OutinterfaceModelDTOobject
     */
    sign?: string;
}
