/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { OutinterfaceGroupDTO } from './outinterface-group-dto';
import { OutinterfaceModelDTO } from './outinterface-model-dto';
/**
 * 分组对象业务对象集合
 * @export
 * @interface OutinterfaceModelAndGroupListDTO
 */
export interface OutinterfaceModelAndGroupListDTO {
    /**
     * 
     * @type {OutinterfaceGroupDTO}
     * @memberof OutinterfaceModelAndGroupListDTO
     */
    groupDTO?: OutinterfaceGroupDTO;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceModelAndGroupListDTO
     */
    groupId?: string;
    /**
     * 业务对象信息集合
     * @type {Array<OutinterfaceModelDTO>}
     * @memberof OutinterfaceModelAndGroupListDTO
     */
    outinterfaceModels?: Array<OutinterfaceModelDTO>;
}
