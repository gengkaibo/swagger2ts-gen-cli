/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 模型信息和所关联模型元数据
 * @export
 * @interface OutinterfaceBasicModelAndMetaDataDTO
 */
export interface OutinterfaceBasicModelAndMetaDataDTO {
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    cName?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    cname?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    columnChineseName?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    columnDescription?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    columnEtlSql?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    columnLength?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    columnName?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    columnPrecision?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    columnType?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    description?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    dimensionMetric?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    dsName?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    enabled?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    metricGroup?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    modelDataSort?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    modelName?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    modelSort?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    modelType?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    statistics?: string;
    /**
     * 
     * @type {string}
     * @memberof OutinterfaceBasicModelAndMetaDataDTO
     */
    tableName?: string;
}
