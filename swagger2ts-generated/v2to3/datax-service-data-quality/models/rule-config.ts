/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: 985134801@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { Accuracy } from './accuracy';
import { Consistent } from './consistent';
import { Relevance } from './relevance';
import { Timeliness } from './timeliness';
import {
    Accuracy,Consistent,Relevance,Timeliness,
} from ".";

/**
 * 
 *
 * @export
 * @interface RuleConfig
 */
export interface RuleConfig {

    /**
     * @type {Accuracy}
     * @memberof RuleConfig
     */
    accuracy?: Accuracy;

    /**
     * @type {Consistent}
     * @memberof RuleConfig
     */
    consistent?: Consistent;

    /**
     * @type {Relevance}
     * @memberof RuleConfig
     */
    relevance?: Relevance;

    /**
     * 核查类型编码
     *
     * @type {string}
     * @memberof RuleConfig
     */
    ruleItemCode?: string;

    /**
     * @type {Timeliness}
     * @memberof RuleConfig
     */
    timeliness?: Timeliness;
}
