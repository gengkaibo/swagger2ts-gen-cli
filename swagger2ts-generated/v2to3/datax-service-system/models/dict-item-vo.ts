/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: 985134801@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import {
    
} from ".";

/**
 * 
 *
 * @export
 * @interface DictItemVo
 */
export interface DictItemVo {

    /**
     * @type {Date}
     * @memberof DictItemVo
     */
    createTime?: Date;

    /**
     * @type {string}
     * @memberof DictItemVo
     */
    dictId?: string;

    /**
     * @type {string}
     * @memberof DictItemVo
     */
    id?: string;

    /**
     * @type {number}
     * @memberof DictItemVo
     */
    itemSort?: number;

    /**
     * @type {string}
     * @memberof DictItemVo
     */
    itemText?: string;

    /**
     * @type {string}
     * @memberof DictItemVo
     */
    itemValue?: string;

    /**
     * @type {string}
     * @memberof DictItemVo
     */
    remark?: string;

    /**
     * @type {string}
     * @memberof DictItemVo
     */
    status?: string;
}
