/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: 985134801@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import {
    
} from ".";

/**
 * 
 *
 * @export
 * @interface Model
 */
export interface Model {

    /**
     * 部门名称
     *
     * @type {string}
     * @memberof Model
     */
    deptName?: string;

    /**
     * 部门编码
     *
     * @type {string}
     * @memberof Model
     */
    deptNo?: string;

    /**
     * 主键ID
     *
     * @type {string}
     * @memberof Model
     */
    id?: string;

    /**
     * 父部门ID
     *
     * @type {string}
     * @memberof Model
     */
    parentId?: string;

    /**
     * 备注
     *
     * @type {string}
     * @memberof Model
     */
    remark?: string;

    /**
     * 状态
     *
     * @type {string}
     * @memberof Model
     */
    status?: string;
}
