/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: 985134801@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import {
    
} from ".";

/**
 * 
 *
 * @export
 * @interface FlowServiceBody
 */
export interface FlowServiceBody {

    /**
     * file
     *
     * @type {Blob}
     * @memberof FlowServiceBody
     */
    file?: Blob;

    /**
     * 详细实体flowService
     *
     * @type {any}
     * @memberof FlowServiceBody
     */
    flowService: any;
}
