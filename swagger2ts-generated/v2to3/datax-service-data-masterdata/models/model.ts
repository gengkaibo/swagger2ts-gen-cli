/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: 985134801@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { Model } from './model';
import {
    Model,
} from ".";

/**
 * 
 *
 * @export
 * @interface Model
 */
export interface Model {

    /**
     * 主键ID
     *
     * @type {string}
     * @memberof Model
     */
    id?: string;

    /**
     * 模型列信息
     *
     * @type {Array<Model>}
     * @memberof Model
     */
    modelColumns?: Array<Model>;

    /**
     * 逻辑表
     *
     * @type {string}
     * @memberof Model
     */
    modelLogicTable?: string;

    /**
     * 模型名称
     *
     * @type {string}
     * @memberof Model
     */
    modelName?: string;

    /**
     * 备注
     *
     * @type {string}
     * @memberof Model
     */
    remark?: string;

    /**
     * 状态
     *
     * @type {string}
     * @memberof Model
     */
    status?: string;
}
