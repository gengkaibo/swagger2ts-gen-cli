/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import {
    
} from ".";

/**
 * 
 *
 * @export
 * @interface JobInfoDto
 */
export interface JobInfoDto {

    /**
     * flinkx任务类型：json、sql
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    flinkxJobType?: string;

    /**
     * 主键ID
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    id?: string;

    /**
     * 增量字段
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    incrColumn?: string;

    /**
     * 是否为实时引接任务
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    isRealTimeJob?: string;

    /**
     * 描述
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    jobDesc?: string;

    /**
     * flinkx运行json
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    jobJson?: string;

    /**
     * 任务名称
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    jobName?: string;

    /**
     * flinkx运行sql
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    jobSql?: string;

    /**
     * 所属项目Id
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    projectId?: string;

    /**
     * 调度配置，值含义取决于调度类型
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    scheduleConf?: string;

    /**
     * 调度类型
     *
     * @type {string}
     * @memberof JobInfoDto
     */
    scheduleType?: string;

    /**
     * 起始位置
     *
     * @type {number}
     * @memberof JobInfoDto
     */
    startLocation?: number;
}
