/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import {
    
} from ".";

/**
 * 
 *
 * @export
 * @interface LogResult
 */
export interface LogResult {

    /**
     * @type {boolean}
     * @memberof LogResult
     */
    end?: boolean;

    /**
     * @type {number}
     * @memberof LogResult
     */
    fromLineNum?: number;

    /**
     * @type {boolean}
     * @memberof LogResult
     */
    isEnd?: boolean;

    /**
     * @type {string}
     * @memberof LogResult
     */
    logContent?: string;

    /**
     * @type {number}
     * @memberof LogResult
     */
    toLineNum?: number;
}
