/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import {
    
} from ".";

/**
 * 
 *
 * @export
 * @interface LindormTsdbWriterDto
 */
export interface LindormTsdbWriterDto {

    /**
     * @type {Array<string>}
     * @memberof LindormTsdbWriterDto
     */
    tags?: Array<string>;

    /**
     * @type {string}
     * @memberof LindormTsdbWriterDto
     */
    timestamp?: string;
}
