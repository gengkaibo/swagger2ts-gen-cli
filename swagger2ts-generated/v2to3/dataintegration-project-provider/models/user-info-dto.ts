/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { ProjectDTO } from './project-dto';
import {
    ProjectDTO,
} from ".";

/**
 * 用户信息
 *
 * @export
 * @interface UserInfoDTO
 */
export interface UserInfoDTO {

    /**
     * 用户名称
     *
     * @type {string}
     * @memberof UserInfoDTO
     */
    describe?: string;

    /**
     * 项目信息
     *
     * @type {Array<ProjectDTO>}
     * @memberof UserInfoDTO
     */
    projectDTOList?: Array<ProjectDTO>;

    /**
     * 用户id
     *
     * @type {string}
     * @memberof UserInfoDTO
     */
    userId?: string;
}
