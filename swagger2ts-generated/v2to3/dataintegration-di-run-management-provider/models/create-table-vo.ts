/* tslint:disable */
/* eslint-disable */
/**
 * API接口文档
 * Api Documentation
 *
 * OpenAPI spec version: 1.0.0
 * Contact: test@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { FieldsInfoVO } from './fields-info-vo';
import {
    FieldsInfoVO,
} from ".";

/**
 * 建表实体类
 *
 * @export
 * @interface CreateTableVO
 */
export interface CreateTableVO {

    /**
     * 数据源id
     *
     * @type {string}
     * @memberof CreateTableVO
     */
    dataSourceId: string;

    /**
     * 字段信息
     *
     * @type {Array<FieldsInfoVO>}
     * @memberof CreateTableVO
     */
    fieldsInfos: Array<FieldsInfoVO>;

    /**
     * 主/技术键字段的名称
     *
     * @type {string}
     * @memberof CreateTableVO
     */
    pk?: string;

    /**
     * 表的schema
     *
     * @type {string}
     * @memberof CreateTableVO
     */
    schema?: string;

    /**
     * 在语句中附加分号
     *
     * @type {boolean}
     * @memberof CreateTableVO
     */
    semicolon: boolean;

    /**
     * 表名
     *
     * @type {string}
     * @memberof CreateTableVO
     */
    tableName: string;

    /**
     * 技术键字段的名称
     *
     * @type {string}
     * @memberof CreateTableVO
     */
    tk?: string;

    /**
     * 如果需要对主键使用自动递增字段，则为true
     *
     * @type {boolean}
     * @memberof CreateTableVO
     */
    useAutoinc: boolean;
}
